# Generated by Django 2.1.7 on 2019-05-04 12:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0011_auto_20190426_1924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='food_item',
            name='course',
            field=models.CharField(choices=[('Starter', 'Starter'), ('Main Course', 'Main Course'), ('Desert', 'Desert')], max_length=100),
        ),
    ]
